import os
import json

from tortworth import db
from tortworth.models import Room

def get_room_data():
    with open(os.path.abspath('tortworth/static/room_list.json')) as room_list:
        room_data = json.load(room_list)
    return room_data

for k, v in get_room_data().items():
    for key, value in v.items():
        if key == 'notes':
            for x in value:
                if 'zip' in x.lower():
                    room = Room.query.get(k)
                    room.zip_link = True
                    db.session.commit()
                    print('*' * 10)
                    print(f'Room {k} set to True')
                else:
                    room = Room.query.get(k)
                    room.zip_link = False
                    db.session.commit()
                    print('*' * 10)
                    print(f'room {k} set to False')

#     room = Room(
#         id=k,
#         armchair= v.get('armchair', 'No') == 'Yes',
#         bathroom_windows= v.get('bathroom_windows', 'No') == 'Yes',
#         coffee_table= v.get('coffe_table', 'No') == 'Yes',
#         connecting= v.get('connecting', 'No') == 'Yes',
#         desk= v.get('desk', 'No') == 'Yes',
#         floor=v.get('floor', 'Not specified'),
#         fridge= v.get('floor', 'No') == 'Yes',
#         roll_bath= v.get('roll_top_bath', 'No') == 'Yes',
#         full_mirror= v.get('full_mirror', 'No') == 'Yes',
#         sofa_bed= v.get('sofa_bed', 'No') == 'Yes',
#         shower_bath= v.get('shower_bath', 'No') == 'Yes',
#         room_type= v.get('room_type', 'ROOM'),
#         size= v.get('size', 'Not specified'),
#         view= v.get('view', 'Not specified')
#     )
#     db.session.add(room)
#     db.session.commit()
#     print(f'Commited room {k}')

