from flask import Blueprint, render_template, redirect, url_for, flash, request
from flask_login import LoginManager, login_user, current_user, logout_user, login_required

from tortworth import utils

mod = Blueprint('index', __name__)

@mod.route('/')
def home():
    if current_user.is_authenticated:
        # return 'Logged in'
        return redirect(url_for('.test'))
    else:
        # return 'Not logged in'
        return redirect(url_for('user.login'))

@mod.route('/test')
def test():
    return render_template('empty.html')
