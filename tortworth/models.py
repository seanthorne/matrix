from tortworth import db
import datetime
from flask_login import UserMixin

def get_model_dict(model):
        return dict((column.name, getattr(model, column.name)) for column in model.__table__.columns)

def to_dict(query_result=None):
    cover_dict = {key: getattr(query_result, key) for key in query_result.items()}
    return cover_dict
 
class User(UserMixin, db.Model):
    """ Create user table"""
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(80), unique=True)
    password = db.Column(db.String(80))
    job_title = db.Column(db.String(80))
    nickname = db.Column(db.String(80))
    admin = db.Column(db.Boolean())
    posts = db.relationship('Post', backref='author', lazy='dynamic')
    notes = db.relationship('RoomNote', backref='room_note_author', lazy='dynamic')
    post_comment = db.relationship('PostComment', backref='post_comment_author', lazy='dynamic')

    def __init__(self, username, password, job_title, admin, nickname):
        self.username = username
        self.password = password
        self.job_title = job_title
        self.admin = admin
        self.nickname = nickname
    def __repr__(self):
        return f'<User: {self.username}'

class Post(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    sentiment = db.Column(db.String(80))
    department = db.Column(db.String(80))
    guest_name = db.Column(db.String(280))
    arrival_date = db.Column(db.DateTime)
    guest_room = db.Column(db.Integer)
    resolution = db.Column(db.String(280))
    cost = db.Column(db.String(30))
    body = db.Column(db.String(280))
    timestamp = db.Column(db.Date, index=True, default=datetime.date.today())
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    post_comment = db.relationship('PostComment', backref='comment', lazy='dynamic')

class PostComment(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    body = db.Column(db.String(280))
    post = db.Column(db.Integer, db.ForeignKey('post.id'))
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    timestamp = db.Column(db.Date, index=True, default=datetime.date.today())

class Room(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    armchair = db.Column(db.Boolean, default=False, nullable=False)
    bathroom_windows = db.Column(db.Boolean, default=False, nullable=False)
    coffee_table = db.Column(db.Boolean, default=False, nullable=False)
    connecting = db.Column(db.Boolean, default=False, nullable=False)
    desk = db.Column(db.Boolean, default=False, nullable=False)
    floor = db.Column(db.String(280))
    fridge = db.Column(db.Boolean, default=False, nullable=False)
    full_mirror = db.Column(db.Boolean, default=False, nullable=False)
    roll_bath = db.Column(db.Boolean, default=False, nullable=False)
    sofa_bed = db.Column(db.Boolean, default=False, nullable=False)
    shower_bath = db.Column(db.Boolean, default=False, nullable=False)
    room_type = db.Column(db.String(10))
    size = db.Column(db.String(10))
    view = db.Column(db.String(280))
    zip_link = db.Column(db.Boolean, default=False )
    notes = db.relationship('RoomNote', backref='room', lazy='dynamic')
    
    def __init__(self, **kwargs):
        super(Room, self).__init__(**kwargs)
 
    def as_dict(self):
        return {c.name: str(getattr(self, c.name)) for c in self.__table__.columns}

class RoomNote(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    body = db.Column(db.String(280))
    room_id = db.Column(db.Integer, db.ForeignKey('room.id'))
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))

class UploadedImage(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    room = db.Column(db.Integer)
    file_name = db.Column(db.String(1000))
    file_path = db.Column(db.String(1000))

    def __init__(self, room, file_name, file_path):
        self.room = room
        self.file_name = file_name
        self.file_path = file_path
