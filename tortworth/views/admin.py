from flask import Blueprint, render_template, flash, request, redirect, url_for, jsonify
from flask_login import current_user, login_required

from tortworth.models import Room
from .. import db

from tortworth.models import User
from tortworth.forms import UpdateUser

mod = Blueprint('admin', __name__)

@mod.route('/')
@login_required
def index():
    admin = User.query.get(current_user.id)
    if not admin.admin:
        flash("You can't do that. Soz.")
        return index.home()

    users = User.query.all()
    return render_template('admin/index.html', users=users)

@mod.route('/<room_number>')
def my(room_number):
    rooms = Room.query.get(room_number)
    return (f"{rooms.id}, {rooms.view}")

@mod.route("/update/<user_id>", methods=['GET', 'POST'])
@login_required
def update_user(user_id):
    form = UpdateUser()
    user = User.query.get(user_id)

    if request.method == 'POST' and form.validate():
        user.username = request.form['email']
        user.password = request.form['password']
        user.job_title = request.form['job_title']
        user.nickname = request.form['nickname']
        if request.form.get('admin') == 'y':
            user.admin = 1
        else:
            user.admin = 0
        try:
            db.session.commit()
        except:
            db.session.rollback()
            flash('Email in use already.')
            return index()
        flash(f'{user.username} updated.')
        return index()
    else:
        return render_template('admin_edit.html', form=form, user=user)


@mod.route('/delete_user/<user_id>')
def delete_user(user_id):
    user = User.query.get(user_id)
    db.session.delete(user)
    db.session.commit()
    flash('Deleted {}'.format(user.id))
    return redirect(url_for('.index'))