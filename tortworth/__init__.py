from flask import Flask, render_template
from flask_sqlalchemy import SQLAlchemy, SessionBase
from flask_migrate import Migrate
from pathlib import Path
import os

app = Flask(__name__)
db = SQLAlchemy(app)

app.secret_key = 'sean'
pn = Path(__file__)
fd = pn.parent.parent / 'test.db'
app.config['SQLALCHEMY_DATABASE_URI'] = f'sqlite:///{fd}'
migrate = Migrate(app, db)

@app.errorhandler(404)
def page_not_found(e):
    return render_template('errors/404.html'), 404

from tortworth.views.routes import mod
from tortworth.views.room import mod
from tortworth.views.admin import mod
from tortworth.views.user import mod
from tortworth.views.log import mod

app.register_blueprint(views.room.mod, url_prefix='/room')
app.register_blueprint(views.log.mod, url_prefix='/log')
app.register_blueprint(views.user.mod, url_prefix='/user')
app.register_blueprint(views.admin.mod, url_prefix='/admin')
app.register_blueprint(views.routes.mod)