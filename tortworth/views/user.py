from flask import Blueprint, flash, render_template, redirect, url_for, request
from flask_login import login_user, logout_user, current_user

from tortworth import db
from tortworth.forms import LoginUser, RegisterUser
from tortworth.models import User, Post
from tortworth.utils import greeting

mod = Blueprint('user', __name__)
@mod.route('/')
def index():
    return 'This is where the ME page would go :)'

@mod.route('/user/<user_id>')
def users_stream(user_id):
    user = User.query.get(user_id)
    posts = Post.query.filter_by(user_id=user.id)
    return render_template('log/posts.html', user=user, posts=posts, form=forms.NewPost())

@mod.route('/profile')
def user_profile():
    user_id = request.args.get('user')
    user = User.query.get(user_id)
    users_posts = Post.query.filter_by(user_id=user_id)
    return render_template(
        'user/posts.html',
        user=user,
        total_posts=users_posts.count(),
        posts=users_posts)

@mod.route('/register', methods=['POST', 'GET'])
def register():
    form = RegisterUser()
    if form.validate_on_submit():
        if form.passcode.data != '1853':
            flash('Passcode is incorrect')
            return redirect(url_for('user.register'))
        new_user = User(
            username = form.email.data,
            password = form.password.data,
            job_title = form.job_title.data,
            nickname = form.nickname.data,
            admin=False
        )
        try:
            db.session.add(new_user)
            db.session.commit()
            flash("Yay! You registered, now login.")
            return redirect(url_for('index.home'))
        except:
            flash('Whoops. I already have that email.')
            return redirect(url_for('user.register'))
        
    return render_template('user/register.html', form=form)

@mod.route("/logout")
def logout():
    """Logout Form"""
    logout_user()
    flash('Successfully logged out.', category='warning')
    return redirect(url_for('index.home'))

@mod.route('/login', methods=['POST', 'GET'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('index.home'))
    form = LoginUser()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.email.data.lower()).first()
        if user is not None and user.password == form.password.data:
            login_user(user=user)
            next = request.args.get('next')
            return redirect(next or url_for('index.home'))
        else:
            flash('Username or password are incorrect.', category='error')
            return redirect(url_for('user.login'))
    return render_template('user/login.html', form=form)