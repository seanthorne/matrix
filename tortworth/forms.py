from flask_wtf import FlaskForm
from datetime import datetime
from wtforms import StringField, PasswordField, BooleanField, SubmitField, IntegerField, HiddenField, MultipleFileField, SelectField, DateTimeField
from wtforms.widgets import TextArea
from wtforms.fields.html5 import DateField

from wtforms.validators import InputRequired, AnyOf, DataRequired, NumberRange, EqualTo, Email, Optional


class EditRoom(FlaskForm):
    floor = StringField('Floor')
    room_type = StringField('room_type')
    view = StringField('view')
    desk = BooleanField('desk')
    armchair = BooleanField('Armchair')
    sitting_area = BooleanField('Sitting Area')
    coffe_table = BooleanField('Coffee Table')
    fridge = BooleanField('Fridge')
    full_mirror = BooleanField('Full Mirror')
    shower_bath = BooleanField('Shower over bath')
    roll_top_bath = BooleanField('Roll top bath')
    bathroom_windows = BooleanField('Bathroom Windows')
    connecting = BooleanField('Connecting')
    size = StringField('Size')
    submit = SubmitField('EDIT')


class NewNote(FlaskForm):
    body = StringField("What's worth a mention?")


class NewPost(FlaskForm):
    body = StringField("Body", [DataRequired(
        message="Can't post an empy post!")], widget=TextArea())
    sentiment = SelectField(
        'Sentiment',
        choices=[('Neutral', 'Neutral'), ('Positive', 'Positive'),
                 ('Negative', 'Negative')],
        validators=[DataRequired(
            message="Sentiment must be positive, negative or neutral.")]
    )
    department = SelectField(
        'Department',
        choices=[
            ('Miscellaneous',
             'Miscellaneous'),
            ('Restaurant',
             '1853 Restaurant'),
            ('Library Bar',
             '1853 Library Bar'),
            ('Atrium',
             'Atrium Bistro'),
            ('Spa',
             'Spa'),
            ('Reception',
             'Reception'),
            ('Housekeeping',
             'Housekeeping'),
            ('Kitchen',
             'Kitchen'),
            ('C&B Ops',
             'C&B Ops'),
            ('C&B Sales',
             'C&B Sales'),
            ('Reservations',
             'Reservations'),
            ('Maintenance',
             'Maintenance'),
            ('Grounds',
             'Grounds')
        ]
    )
    guest_name = StringField('Guest\'s name')
    arrival_date = DateField(
        "Guest's arrival date", validators=[Optional()], format='%Y-%m-%d')
    guest_room = StringField("Guest's Bedroom")
    resolution = StringField("Resolution", widget=TextArea())
    cost = StringField('Cost')
    submit = SubmitField('Create new entry')


class UploadFile(FlaskForm):
    file = MultipleFileField(
        validators=[DataRequired(message="Please choose an image.")])

# class ChangeDate(FlaskForm)


class RemindUser(FlaskForm):
    password = StringField('What is your email address?', [Email('Erm. That\'s not an email address.'), EqualTo(
        'confirm_password', message="Erm. Your emails don't match?")])
    confirm_password = StringField('Can you repeat your email, please?',)
    submit = SubmitField('Email me')


class LoginUser(FlaskForm):
    email = StringField('Email', [Email(message='Email is invalid.')])
    password = PasswordField(
        [DataRequired(message="Please input a password.")])

class PostCommentForm(FlaskForm):
    body = StringField("Comment", widget=TextArea())
    submit = SubmitField('Add Comment')


class UpdateUser(FlaskForm):
    email = StringField('Email', validators=[
                        Email(message='Not a valid email address.')])
    password = StringField('New Password',  [DataRequired(message="No Password"), EqualTo(
        'confirm_password', message="Passwords do not match")])
    confirm_password = StringField('Retype Password')
    job_title = StringField('Job Title')
    nickname = StringField('Nickname')
    admin = BooleanField('Admin')
    submit = SubmitField('Update')


class RegisterUser(FlaskForm):
    email = StringField('What is your email address?', [
                        DataRequired(), Email(message="Not a valid Email Address.")])
    username = HiddenField()
    password = PasswordField('What would you like your password to be?',  [DataRequired(
        message="No Password"), EqualTo('confirm_password', message="Passwords do not match")])
    confirm_password = PasswordField(
        'Please retype your password, to check for typos!')
    job_title = StringField('What is your job title?', [
                            DataRequired('Not a valid job title.')])
    nickname = StringField('Nickname', [
                            DataRequired('You need to enter a nickname.')])
    passcode = StringField('Please enter the passcode', [DataRequired('Not a password is incorrect')])
    submit = SubmitField('Register')
