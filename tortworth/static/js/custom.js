
function deletePost(post) {
    $('#' + post).addClass('lightSpeedOut animated font-weight-bold');
};

$(document).ready(function () {
    $('#entryDepartment').click(function () {
        var selectedEntry = $('#entryDepartment').find(":selected").text();
        if (selectedEntry == 'Maintenance') {
            console.log('fix it ')
            $('#departmentHelp').removeClass('text-green').addClass('text-red font-weight-bold').html("Has this been added to Opera?");
        } else {
            $('#departmentHelp').addClass('text-green font-weight-bold').html("Thank you.");
        };
    })
    $("#goToDate").click(function () {
        $("#dateLoader").removeClass('d-none');
    });
    $('#entrySentiment').click(function () {
        $('#sentimentHelp').addClass('text-green font-weight-bold').html("Thank you.");
        var selectedEntry = $('#entrySentiment').find(":selected").text();
        if (selectedEntry == 'Neutral') {
            $('#entrySentiment').addClass('border-warning').removeClass('border-success border-danger');
        }
        if (selectedEntry == 'Negative') {
            $('#entrySentiment').addClass('border-danger').removeClass('border-success border-warning');
        } if (selectedEntry == 'Positive') {
            $('#entrySentiment').addClass('border-success').removeClass('border-danger border-warning');
        };
    }
    )
    // $('#entryDepartment').click(function () {
    //     $('#departmentHelp').addClass('text-green font-weight-bold').html("Thank you.");
    // }
    // )


    // collapses room notes on mobile
    $(window).on("load, resize", function () {
        var viewportWidth = $(window).width();
        if (viewportWidth < 600) {
            $("#roomNotes").addClass("card-collapsed");
        }
    });

});
$(document).ready(function () {
    /** Constant div card */
    const DIV_CARD = 'div.card';

    /** Initialize tooltips */
    $('[data-toggle="tooltip"]').tooltip();

    /** Initialize popovers */
    $('[data-toggle="popover"]').popover({
        html: true
    });

    /** Function for remove card */
    $('[data-toggle="card-remove"]').on('click', function (e) {
        let $card = $(this).closest(DIV_CARD);

        $card.remove();

        e.preventDefault();
        return false;
    });

    /** Function for collapse card */
    $('[data-toggle="card-collapse"]').on('click', function (e) {
        let $card = $(this).closest(DIV_CARD);

        $card.toggleClass('card-collapsed');

        e.preventDefault();
        return false;
    });

    /** Function for fullscreen card */
    $('[data-toggle="card-fullscreen"]').on('click', function (e) {
        let $card = $(this).closest(DIV_CARD);

        $card.toggleClass('card-fullscreen').removeClass('card-collapsed');

        e.preventDefault();
        return false;
    });
});
$(window).on('resize', function () {
    var win = $(this);
    if (win.width() < 514) {

        $('#roomNotes').addClass('card-collapsed');

    }
    else {
        $('#roomNotes').removeClass('card-collapsed');
    }

});
