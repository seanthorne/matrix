from tortworth import app, db
from tortworth.models import User
from flask_login import LoginManager, login_user, current_user, logout_user, login_required

login = LoginManager(app)
login.init_app(app)
login.login_view = "user.login"

@login.user_loader
def load_user(id):
    return User.query.get(int(id))

if __name__ == "__main__":
    app.run(debug=True)