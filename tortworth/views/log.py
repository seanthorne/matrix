from flask import Blueprint, render_template, flash, redirect, url_for, request
from flask_login import current_user, login_required

from tortworth import db
from tortworth.models import Post, PostComment
from tortworth.forms import NewPost, PostCommentForm

import datetime
from datetime import timedelta

mod = Blueprint('log', __name__)

@mod.route('/')
@mod.route('/today')
@login_required
def index():
    posts = Post.query.filter_by(timestamp=datetime.date.today()).all()
    form = PostCommentForm()
    comments = PostComment.query.all()
    return render_template('log/main.html', posts=posts, date=datetime.date.today(), form=form, comments=comments)

@mod.route('/yesterday')
@login_required
def yesterdays_log():
    posts = Post.query.filter_by(
        timestamp=datetime.date.today() - timedelta(days=1)).all()
    form = PostCommentForm()

    return render_template('log/main.html', posts=posts, date=datetime.date.today() - timedelta(days=1), form=form)

@mod.route('/date', methods=['POST', 'GET'])
def pick_log_date():
    if request.form:
        date = request.form['date']
    elif request.args:
        date = request.args.get('date')
    else:
        return 'None'
    dt = datetime.datetime.strptime(str(date), '%Y-%m-%d')
    posts = Post.query.filter_by(timestamp=dt.date()).all()
    form = PostCommentForm()
    return render_template('log/main.html', posts=posts, date=dt.date(), form=form)

@mod.route('/delete/<post_id>')
def delete(post_id):
    post_to_delete = Post.query.get(post_id)
    db.session.delete(post_to_delete)
    db.session.commit()
    return (' ', 204)

@mod.route('/comment/<post_id>', methods=['POST'])
def comment(post_id):
    form = PostCommentForm()
    if form.validate_on_submit():
        new_post = PostComment(
            body = form.body.data,
            user_id = int(current_user.id),
            post = int(post_id)
            )
        db.session.add(new_post)
        db.session.commit()
        return redirect(url_for('.index'))
    return 'nope'

@mod.route('/create/<date>', methods=['GET', 'POST'])
@login_required
def create(date):
    dt = datetime.datetime.strptime(date, '%Y-%m-%d')
    form = NewPost()
    if form.validate_on_submit():
        new_post = Post(
            body=form.body.data,
            user_id=current_user.id,
            sentiment=form.sentiment.data,
            department=form.department.data,
            guest_name=form.guest_name.data,
            arrival_date=form.arrival_date.data,
            guest_room = form.guest_room.data,
            resolution = form.resolution.data,
            cost = form.cost.data,
            timestamp=dt.date())
        db.session.add(new_post)
        db.session.commit()
        return redirect(url_for('.pick_log_date', date=dt.date()))

    return render_template('log/posts.html', date=dt.date(), form=form)