from flask import Blueprint, redirect, flash, request, render_template, url_for, session
from flask_login import current_user, login_required
from tortworth import db

from tortworth.utils import *
from tortworth.models import UploadedImage, Room, get_model_dict, RoomNote, to_dict
from tortworth.forms import UploadFile, NewNote, EditRoom
from werkzeug import secure_filename

import uuid


mod = Blueprint('room', __name__)

@mod.route('/test')
def homepage():
    return 'You are on the homepage'

@mod.route('/search', methods=['POST','GET'])
def search():
    rooms = Room.query.all()
    if not request.form:
        return render_template('room/search.html', rooms=rooms)
    room = request.form.get('Room')
    room = room.lstrip('0')
    if room == '':
        flash('No room entered.')
        return redirect(url_for('room.search'))
    elif all(x.isalpha() or x.isspace() for x in room):
        flash('Please enter room number, not name.')
        return redirect(url_for('room.search'))
    search = Room.query.get(room)
    if search:
        return redirect('/room/{}'.format(room))
    else:
        flash('Room not found')
        return redirect(url_for('room.search'))

@mod.route('/upload/<room_number>', methods=['GET', 'POST'])
def images(room_number):
    form = UploadFile()
    all_images = UploadedImage.query.filter_by(room=room_number).all()
    length = len(all_images)
    file_paths = []
    for x in all_images:
        file_paths.append(f"/static/uploads/{x.file_name}")
    if length == 0:
        file_paths = False
    if form.validate_on_submit():
        f = request.files.getlist('file')
        for image in f:
            image_file_name = f'{room_number}-' + \
                str(uuid.uuid4()) + secure_filename(image.filename)
            image_file = os.path.join(os.path.abspath('tortworth/static/uploads'), image_file_name)
            image.save(image_file)
            image_upload = UploadedImage(
                room=room_number,
                file_name=image_file_name,
                file_path=image_file
            )
            db.session.add(image_upload)
        db.session.commit()
        db.session.close()
        flash('Images added.')
        return redirect(url_for('room.search'))

    return render_template('room/upload.html', room=room_number, images=all_images, form=form)

@mod.route('/edit/<room_number>')
def edit(room_number):
    room = Room.query.get(room_number)
    form = EditRoom(obj=room)
    return render_template('room/edit.html', form=form)

@mod.route('/test')
def delete():
    pass

@mod.route('/<room_number>')
def room(room_number):
    all_images = UploadedImage.query.filter_by(room=room_number).all()
    file_paths = []
    for x in all_images:
        file_paths.append(f"{x.file_name}")
    if len(all_images) == 0:
        file_paths = False
    room = Room.query.get(room_number)
    notes = RoomNote.query.filter_by(room_id=room_number)
    print(type(notes.count()))
    if notes.count() == 0:
        notes = False
    form = NewNote()
    return render_template('room/room.html', notes=notes, room=room, form=form, images=file_paths)


@mod.route('/note/add/<room_number>', methods=['POST', 'GET'])
def add_note(room_number):
    form = NewNote()
    if form.validate_on_submit():
        new_note = RoomNote(
            body = form.body.data,
            user_id = current_user.id,
            room_id = room_number
        )
        db.session.add(new_note)
        db.session.commit()
        flash('Note added')
        return redirect(url_for('.room', room_number=room_number))
    return render_template('room/add_note.html', form=form, room=room_number)